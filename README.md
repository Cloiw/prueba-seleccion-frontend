## Installation
1. Install dependencies

```sh
$ npm install
```

2. Initialize application
```sh
 $ npm start
  ```

App should be running at:
```sh
 http://localhost:3000/
  ```

#### Login and register
To simulate registration or login you can use these values:

    email: eve.holt@reqres.in
    password: pistol
