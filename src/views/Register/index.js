import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Alert } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import EmailPasswordForm from '../../components/EmailPasswordForm';
import * as userActions from '../../store/slices/userSlice';

const Register = ({ register, error, loading, success, resetSuccess, resetError }) => {
  const [showSuccess, setShowSuccess] = useState(false);

  const handleSuccess = () => {
    resetSuccess(false);
    setShowSuccess(true);
  };

  useEffect(() => {
    if (success) {
      handleSuccess();
    }
  });
  return (
    <>
      <h1>Create account</h1>
      <hr />
      {showSuccess ? (
        <Alert variant="success">Success! You can now login</Alert>
      ) : (
        <>
          <div className="error-container">
            {error.type && <Alert variant="danger">{error.type}</Alert>}
          </div>
          <EmailPasswordForm
            buttonName="Sign up"
            isLoading={loading}
            onSubmit={register}
          />
        </>
      )}
      <hr />
      <span>Back to </span>
      <Link onClick={() => resetError({})} className="main-link" to="/login">
        login
      </Link>
    </>
  );
};

const mapStateToProps = state => ({
  loading: state.user.registerLoading,
  success: state.user.registerSuccess,
  error: state.user.registerError,
});

const mapDispatchToProps = dispatch => ({
  register: ({ email, password }) => {
    dispatch(userActions.userRegister({ email, password }));
  },
  resetError: error => {
    dispatch(userActions.registerFailure(error));
  },
  resetSuccess: status => {
    dispatch(userActions.registerSuccess(status));
  },
});

Register.propTypes = {
  error: PropTypes.oneOfType([PropTypes.object]).isRequired,
  register: PropTypes.func.isRequired,
  resetError: PropTypes.func.isRequired,
  resetSuccess: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  success: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
