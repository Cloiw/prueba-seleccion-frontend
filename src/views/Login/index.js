import React from 'react';
import { Link } from 'react-router-dom';
import { Alert } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import EmailPasswordForm from '../../components/EmailPasswordForm';
import * as userActions from '../../store/slices/userSlice';

const Login = ({ login, error, loading, cleanError }) => {
  return (
    <>
      <h1>Login</h1>
      <hr />
      <div className="error-container">
        {error.type && <Alert variant="danger">{error.type}</Alert>}
      </div>
      <EmailPasswordForm isLoading={loading} onSubmit={login} />
      <hr />
      <span>Doesn&apos;t have an account yet? </span>
      <Link onClick={() => cleanError({})} className="main-link" to="/register">
        Sign up
      </Link>
    </>
  );
};

const mapStateToProps = state => ({
  loading: state.user.authLoading,
  error: state.user.authError,
});

const mapDispatchToProps = dispatch => ({
  login: ({ email, password }) => {
    dispatch(userActions.userAuth({ email, password }));
  },
  cleanError: error => {
    dispatch(userActions.authFailure(error));
  },
});

Login.propTypes = {
  error: PropTypes.oneOfType([PropTypes.object]).isRequired,
  login: PropTypes.func.isRequired,
  cleanError: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
