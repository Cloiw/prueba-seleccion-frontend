import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col, Spinner, Image } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import BackButton from '../../components/BackButton';
import * as charactersActions from '../../store/slices/charactersSlice';
import './DetailCharacter.scss';

const DetailCharacter = ({ getSingleCharacter, character, loading }) => {
  const { id } = useParams();
  useEffect(() => {
    getSingleCharacter(id);
  }, [getSingleCharacter, id]);

  return (
    <Row>
      <Col>
        <Row className="justify-content-center p-5">
          {loading ? (
            <Spinner animation="grow" variant="light" />
          ) : (
            <>
              <Col md={6} xs={12} className="d-flex justify-content-center">
                <Image className="detail-image" src={character.image} />
              </Col>
              <Col md={6} xs={12} className="my-4 detail-text">
                <p>
                  <span className="text-blue">Name: </span>
                  {character.name}
                </p>
                <p>
                  <span className="text-blue">Status: </span> {character.status}
                </p>
                <p>
                  <span className="text-blue">Species: </span> {character.species}
                </p>
                <p>
                  <span className="text-blue">Gender: </span> {character.gender}
                </p>
                <p>
                  <span className="text-blue">Origin: </span>
                  {character.origin.name}
                </p>
                <p>
                  <span className="text-blue">Location: </span> {character.location.name}
                </p>
              </Col>
            </>
          )}
        </Row>
        <BackButton />
      </Col>
    </Row>
  );
};

const mapStateToProps = state => ({
  character: state.characters.singleCharacter,
  loading: state.characters.singleCharacterLoading,
});

const mapDispatchToProps = dispatch => ({
  getSingleCharacter: id => {
    dispatch(charactersActions.getSingleCharacter(id));
  },
});

DetailCharacter.propTypes = {
  getSingleCharacter: PropTypes.func.isRequired,
  character: PropTypes.oneOfType([PropTypes.object]).isRequired,
  loading: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailCharacter);
