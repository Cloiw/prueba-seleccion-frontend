import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col, Spinner } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import BackButton from '../../components/BackButton';
import * as locationsActions from '../../store/slices/locationsSlice';

const DetailLocation = ({ getSingleLocation, location, loading }) => {
  const { id } = useParams();
  useEffect(() => {
    getSingleLocation(id);
  }, [getSingleLocation, id]);

  return (
    <Row>
      <Col>
        <Row className="justify-content-center p-5">
          {loading ? (
            <Spinner animation="grow" variant="light" />
          ) : (
            <>
              <Col md={6} xs={12} className="my-4 detail-text">
                <p>
                  <span className="text-blue">Name: </span>
                  {location.name}
                </p>
                <p>
                  <span className="text-blue">Type: </span> {location.type}
                </p>
                <p>
                  <span className="text-blue">Dimension: </span> {location.dimension}
                </p>
                <p>
                  <span className="text-blue">N° residents: </span>
                  {location.residents.length}
                </p>
              </Col>
            </>
          )}
        </Row>
        <BackButton />
      </Col>
    </Row>
  );
};

const mapStateToProps = state => ({
  location: state.locations.singleLocation,
  loading: state.locations.singleLocationLoading,
});

const mapDispatchToProps = dispatch => ({
  getSingleLocation: id => {
    dispatch(locationsActions.getSingleLocation(id));
  },
});

DetailLocation.propTypes = {
  getSingleLocation: PropTypes.func.isRequired,
  location: PropTypes.oneOfType([PropTypes.object]).isRequired,
  loading: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailLocation);
