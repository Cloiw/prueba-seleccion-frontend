import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Tabs, Tab } from 'react-bootstrap';
import { connect } from 'react-redux';
import Characters from '../../components/Characters';
import Locations from '../../components/Locations';
import Episodes from '../../components/Episodes';
import * as homeActions from '../../store/slices/homeSlice';

const Home = ({ currentTab, setCurrentTab }) => {
  return (
    <Row>
      <Col className="p-5 main-tabs">
        <Tabs id="controlled-tab" activeKey={currentTab} onSelect={k => setCurrentTab(k)}>
          <Tab eventKey="characters" title="Characters">
            <Characters />
          </Tab>
          <Tab eventKey="locations" title="Locations">
            <Locations />
          </Tab>
          <Tab eventKey="episodes" title="Episodes">
            <Episodes />
          </Tab>
        </Tabs>
      </Col>
    </Row>
  );
};

const mapStateToProps = state => ({
  currentTab: state.home.currentTab,
});

const mapDispatchToProps = dispatch => ({
  setCurrentTab: key => {
    dispatch(homeActions.setCurrentTab(key));
  },
});

Home.propTypes = {
  currentTab: PropTypes.string.isRequired,
  setCurrentTab: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
