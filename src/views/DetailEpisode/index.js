import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col, Spinner } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import BackButton from '../../components/BackButton';
import * as episodesActions from '../../store/slices/episodesSlice';

const DetailEpisode = ({ getSingleEpisode, episode, loading }) => {
  const { id } = useParams();
  useEffect(() => {
    getSingleEpisode(id);
  }, [getSingleEpisode, id]);

  return (
    <Row>
      <Col>
        <Row className="justify-content-center p-5">
          {loading ? (
            <Spinner animation="grow" variant="light" />
          ) : (
            <>
              <Col md={6} xs={12} className="my-4 detail-text">
                <p>
                  <span className="text-blue">Episode number: </span>
                  {episode.episode.slice(-2)}
                </p>
                <p>
                  <span className="text-blue">Season: </span>
                  {episode.episode.substr(2, 1)}
                </p>
                <p>
                  <span className="text-blue">Name: </span>
                  {episode.name}
                </p>
                <p>
                  <span className="text-blue">Air date: </span> {episode.air_date}
                </p>
                <p>
                  <span className="text-blue">N° characters: </span>
                  {episode.characters.length}
                </p>
              </Col>
            </>
          )}
        </Row>
        <BackButton />
      </Col>
    </Row>
  );
};

const mapStateToProps = state => ({
  episode: state.episodes.singleEpisode,
  loading: state.episodes.singleEpisodeLoading,
});

const mapDispatchToProps = dispatch => ({
  getSingleEpisode: id => {
    dispatch(episodesActions.getSingleEpisode(id));
  },
});

DetailEpisode.propTypes = {
  getSingleEpisode: PropTypes.func.isRequired,
  episode: PropTypes.oneOfType([PropTypes.object]).isRequired,
  loading: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailEpisode);
