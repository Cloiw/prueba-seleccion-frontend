import { createSlice } from '@reduxjs/toolkit';
import rickAndMortyService from '../../services/rickAndMorty';

const locationsSlice = createSlice({
  name: 'location',
  initialState: {
    locationsList: [],
    totalDocs: 0,
    currentPage: 1,
    searchValue: '',
    locationsError: {},
    locationsLoading: false,
    singleLocation: {},
    singleLocationError: {},
    singleLocationLoading: true,
  },
  reducers: {
    locationsRequest(state) {
      return {
        ...state,
        locationsLoading: true,
        locationsError: {},
      };
    },
    locationsSuccess(state, action) {
      return {
        ...state,
        locationsLoading: false,
        locationsList: action.payload.results,
        totalDocs: action.payload.totalDocs,
      };
    },
    locationsFailure(state, action) {
      return {
        ...state,
        locationsLoading: false,
        locationsError: action.payload,
      };
    },
    singleLocationRequest(state) {
      return {
        ...state,
        singleLocationLoading: true,
        singleLocationError: {},
      };
    },
    singleLocationSuccess(state, action) {
      return {
        ...state,
        singleLocationLoading: false,
        singleLocation: action.payload,
      };
    },
    singleLocationFailure(state, action) {
      return {
        ...state,
        singleLocationLoading: false,
        singleLocationError: action.payload,
      };
    },
    setCurrentPage(state, action) {
      return {
        ...state,
        currentPage: action.payload,
      };
    },
    setSearchValue(state, action) {
      return {
        ...state,
        searchValue: action.payload,
      };
    },
  },
});

export const {
  locationsRequest,
  locationsSuccess,
  locationsFailure,
  singleLocationRequest,
  singleLocationSuccess,
  singleLocationFailure,
  setCurrentPage,
  setSearchValue,
} = locationsSlice.actions;

export const getLocations = (currentPage, filterName) => async dispatch => {
  dispatch(locationsRequest());
  try {
    const { response, status } = await rickAndMortyService.getLocations(
      currentPage,
      filterName
    );
    if (status === 200) {
      dispatch(
        locationsSuccess({ results: response.results, totalDocs: response.info.count })
      );
    } else {
      dispatch(
        locationsFailure({
          type: response.error,
        })
      );
    }
  } catch (error) {
    dispatch(
      locationsFailure({
        type: error.message,
      })
    );
  }
};

export const getSingleLocation = id => async dispatch => {
  dispatch(singleLocationRequest());
  try {
    const { response } = await rickAndMortyService.getSingleLocation(id);
    dispatch(singleLocationSuccess(response));
  } catch (error) {
    dispatch(
      singleLocationFailure({
        type: error.message,
      })
    );
  }
};

export default locationsSlice.reducer;
