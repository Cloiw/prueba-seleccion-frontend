import { createSlice } from '@reduxjs/toolkit';
import rickAndMortyService from '../../services/rickAndMorty';

const charactersSlice = createSlice({
  name: 'characters',
  initialState: {
    charactersList: [],
    totalDocs: 0,
    currentPage: 1,
    searchValue: '',
    charactersError: {},
    charactersLoading: false,
    singleCharacter: { origin: { name: '' }, location: { name: '' } },
    singleCharacterError: {},
    singleCharacterLoading: true,
  },
  reducers: {
    charactersRequest(state) {
      return {
        ...state,
        charactersLoading: true,
        charactersError: {},
      };
    },
    charactersSuccess(state, action) {
      return {
        ...state,
        charactersLoading: false,
        charactersList: action.payload.results,
        totalDocs: action.payload.totalDocs,
      };
    },
    charactersFailure(state, action) {
      return {
        ...state,
        charactersLoading: false,
        charactersError: action.payload,
      };
    },
    singleCharacterRequest(state) {
      return {
        ...state,
        singleCharacterLoading: true,
        singleCharacterError: {},
      };
    },
    singleCharacterSuccess(state, action) {
      return {
        ...state,
        singleCharacterLoading: false,
        singleCharacter: action.payload,
      };
    },
    singleCharacterFailure(state, action) {
      return {
        ...state,
        singleCharacterLoading: false,
        singleCharacterError: action.payload,
      };
    },
    setCurrentPage(state, action) {
      return {
        ...state,
        currentPage: action.payload,
      };
    },
    setSearchValue(state, action) {
      return {
        ...state,
        searchValue: action.payload,
      };
    },
  },
});

export const {
  charactersRequest,
  charactersSuccess,
  charactersFailure,
  singleCharacterRequest,
  singleCharacterSuccess,
  singleCharacterFailure,
  setCurrentPage,
  setSearchValue,
} = charactersSlice.actions;

export const getCharacters = (currentPage, filterName) => async dispatch => {
  dispatch(charactersRequest());
  try {
    const { response, status } = await rickAndMortyService.getCharacters(
      currentPage,
      filterName
    );
    if (status === 200) {
      dispatch(
        charactersSuccess({ results: response.results, totalDocs: response.info.count })
      );
    } else {
      dispatch(
        charactersFailure({
          type: response.error,
        })
      );
    }
  } catch (error) {
    dispatch(
      charactersFailure({
        type: error.message,
      })
    );
  }
};

export const getSingleCharacter = id => async dispatch => {
  dispatch(singleCharacterRequest());
  try {
    const { response } = await rickAndMortyService.getSingleCharacter(id);
    dispatch(singleCharacterSuccess(response));
  } catch (error) {
    dispatch(
      singleCharacterFailure({
        type: error.message,
      })
    );
  }
};

export default charactersSlice.reducer;
