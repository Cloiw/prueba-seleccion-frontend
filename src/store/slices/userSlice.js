import { createSlice } from '@reduxjs/toolkit';
import userService from '../../services/user';

const userSlice = createSlice({
  name: 'user',
  initialState: {
    isUserAuthenticated: !!localStorage.getItem('RickAndMortyAuth'),
    authError: {},
    authLoading: false,
    registerError: {},
    registerLoading: false,
    registerSuccess: false,
  },
  reducers: {
    authRequest(state) {
      return {
        ...state,
        authLoading: true,
        authError: {},
      };
    },
    authSuccess(state) {
      return {
        ...state,
        authLoading: false,
        isUserAuthenticated: true,
      };
    },
    authFailure(state, action) {
      return {
        ...state,
        authLoading: false,
        authError: action.payload,
      };
    },
    logout(state) {
      return {
        ...state,
        isUserAuthenticated: false,
      };
    },
    registerRequest(state) {
      return {
        ...state,
        registerLoading: true,
        registerError: {},
      };
    },
    registerSuccess(state, action) {
      return {
        ...state,
        registerLoading: false,
        registerSuccess: action.payload,
      };
    },
    registerFailure(state, action) {
      return {
        ...state,
        registerLoading: false,
        registerError: action.payload,
      };
    },
  },
});

export const {
  authRequest,
  authSuccess,
  authFailure,
  registerRequest,
  registerSuccess,
  registerFailure,
  logout,
} = userSlice.actions;

export const userAuth = ({ email, password }) => async dispatch => {
  dispatch(authRequest());
  try {
    const { response, status } = await userService.userAuth({
      email,
      password,
    });
    if (status === 200) {
      const { token } = response;
      window.localStorage.setItem('RickAndMortyAuth', token);
      dispatch(authSuccess());
    } else {
      dispatch(
        authFailure({
          type: response.error,
        })
      );
    }
  } catch (error) {
    dispatch(
      authFailure({
        type: error.message,
      })
    );
  }
};

export const logoutUser = () => dispatch => {
  localStorage.clear();
  dispatch(logout());
};

export const userRegister = ({ email, password }) => async dispatch => {
  dispatch(registerRequest());
  try {
    const { response, status } = await userService.userRegister({
      email,
      password,
    });
    if (status === 200) {
      dispatch(registerSuccess(true));
    } else {
      dispatch(
        registerFailure({
          type: response.error,
        })
      );
    }
  } catch (error) {
    dispatch(
      registerFailure({
        type: error.message,
      })
    );
  }
};

export default userSlice.reducer;
