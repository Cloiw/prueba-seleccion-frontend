import { createSlice } from '@reduxjs/toolkit';
import rickAndMortyService from '../../services/rickAndMorty';

const episodesSlice = createSlice({
  name: 'episodes',
  initialState: {
    searchValue: '',
    episodesFirstSeasonError: {},
    episodesFirstSeasonLoading: false,
    episodesFirstSeasonList: [],
    episodesSecondSeasonError: {},
    episodesSecondSeasonLoading: false,
    episodesSecondSeasonList: [],
    episodesThirdSeasonError: {},
    episodesThirdSeasonLoading: false,
    episodesThirdSeasonList: [],
    singleEpisodeError: {},
    singleEpisodeLoading: false,
    singleEpisode: { characters: [], episode: '' },
    filteredFirstSeason: [],
    filteredSecondSeason: [],
    filteredThirdSeason: [],
  },
  reducers: {
    episodesFirstSeasonRequest(state) {
      return {
        ...state,
        episodesFirstSeasonLoading: true,
        episodesFirstSeasonError: {},
      };
    },
    episodesFirstSeasonSuccess(state, action) {
      return {
        ...state,
        episodesFirstSeasonLoading: false,
        episodesFirstSeasonList: action.payload,
      };
    },
    episodesFirstSeasonFailure(state, action) {
      return {
        ...state,
        episodesFirstSeasonLoading: false,
        episodesFirstSeasonError: action.payload,
      };
    },
    episodesSecondSeasonRequest(state) {
      return {
        ...state,
        episodesSecondSeasonLoading: true,
        episodesSecondSeasonError: {},
      };
    },
    episodesSecondSeasonSuccess(state, action) {
      return {
        ...state,
        episodesSecondSeasonLoading: false,
        episodesSecondSeasonList: action.payload,
      };
    },
    episodesSecondSeasonFailure(state, action) {
      return {
        ...state,
        episodesSecondSeasonLoading: false,
        episodesSecondSeasonError: action.payload,
      };
    },
    episodesThirdSeasonRequest(state) {
      return {
        ...state,
        episodesThirdSeasonLoading: true,
        episodesThirdSeasonError: {},
      };
    },
    episodesThirdSeasonSuccess(state, action) {
      return {
        ...state,
        episodesThirdSeasonLoading: false,
        episodesThirdSeasonList: action.payload,
      };
    },
    episodesThirdSeasonFailure(state, action) {
      return {
        ...state,
        episodesThirdSeasonLoading: false,
        episodesThirdSeasonError: action.payload,
      };
    },
    singleEpisodeRequest(state) {
      return {
        ...state,
        singleEpisodeLoading: true,
        singleEpisodeError: {},
      };
    },
    singleEpisodeSuccess(state, action) {
      return {
        ...state,
        singleEpisodeLoading: false,
        singleEpisode: action.payload,
      };
    },
    singleEpisodeFailure(state, action) {
      return {
        ...state,
        singleEpisodeLoading: false,
        singleEpisodeError: action.payload,
      };
    },
    setSearchValue(state, action) {
      const firstSeason = [...state.episodesFirstSeasonList];
      const secondSeason = [...state.episodesSecondSeasonList];
      const thirdSeason = [...state.episodesThirdSeasonList];

      const filterFirstSeason = firstSeason.filter(element =>
        element.name.toLowerCase().includes(action.payload)
      );
      const filterSecondSeason = secondSeason.filter(element =>
        element.name.toLowerCase().includes(action.payload)
      );
      const filterThirdSeason = thirdSeason.filter(element =>
        element.name.toLowerCase().includes(action.payload)
      );
      return {
        ...state,
        searchValue: action.payload,
        filteredFirstSeason: filterFirstSeason,
        filteredSecondSeason: filterSecondSeason,
        filteredThirdSeason: filterThirdSeason,
      };
    },
  },
});

export const {
  episodesFirstSeasonRequest,
  episodesFirstSeasonSuccess,
  episodesFirstSeasonFailure,
  episodesSecondSeasonRequest,
  episodesSecondSeasonSuccess,
  episodesSecondSeasonFailure,
  episodesThirdSeasonRequest,
  episodesThirdSeasonSuccess,
  episodesThirdSeasonFailure,
  singleEpisodeRequest,
  singleEpisodeSuccess,
  singleEpisodeFailure,
  setSearchValue,
} = episodesSlice.actions;

export const getFirstSeasonEpisodes = () => async dispatch => {
  dispatch(episodesFirstSeasonRequest());
  try {
    const { response } = await rickAndMortyService.getEpisodes([
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
    ]);
    dispatch(episodesFirstSeasonSuccess(response));
  } catch (error) {
    dispatch(
      episodesFirstSeasonFailure({
        type: error.message,
      })
    );
  }
};

export const getSecondSeasonEpisodes = () => async dispatch => {
  dispatch(episodesSecondSeasonRequest());
  try {
    const { response } = await rickAndMortyService.getEpisodes([
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
    ]);
    dispatch(episodesSecondSeasonSuccess(response));
  } catch (error) {
    dispatch(
      episodesSecondSeasonFailure({
        type: error.message,
      })
    );
  }
};

export const getThirdSeasonEpisodes = () => async dispatch => {
  dispatch(episodesThirdSeasonRequest());
  try {
    const { response } = await rickAndMortyService.getEpisodes([
      22,
      23,
      24,
      25,
      26,
      27,
      28,
      29,
      30,
      31,
    ]);
    dispatch(episodesThirdSeasonSuccess(response));
  } catch (error) {
    dispatch(
      episodesThirdSeasonFailure({
        type: error.message,
      })
    );
  }
};

export const getSingleEpisode = id => async dispatch => {
  dispatch(singleEpisodeRequest());
  try {
    const { response } = await rickAndMortyService.getSingleEpisode(id);
    dispatch(singleEpisodeSuccess(response));
  } catch (error) {
    dispatch(
      singleEpisodeFailure({
        type: error.message,
      })
    );
  }
};

export default episodesSlice.reducer;
