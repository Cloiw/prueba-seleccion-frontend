import { createSlice } from '@reduxjs/toolkit';

const homeSlice = createSlice({
  name: 'home',
  initialState: {
    currentTab: 'characters',
  },
  reducers: {
    setCurrentTab(state, action) {
      return {
        currentTab: action.payload,
      };
    },
  },
});

export const { setCurrentTab } = homeSlice.actions;

export default homeSlice.reducer;
