import { configureStore } from '@reduxjs/toolkit';
import home from './slices/homeSlice';
import user from './slices/userSlice';
import characters from './slices/charactersSlice';
import locations from './slices/locationsSlice';
import episodes from './slices/episodesSlice';

export default configureStore({
  reducer: {
    home,
    user,
    characters,
    locations,
    episodes,
  },
});
