import axios from 'axios';

const userAuth = async ({ email, password }) => {
  const response = await axios({
    method: 'post',
    url: 'https://reqres.in/api/login',
    data: {
      email,
      password,
    },
    validateStatus: status => status >= 200 && status <= 400,
  });
  return { response: response.data, status: response.status };
};

const userRegister = async ({ email, password }) => {
  const response = await axios({
    method: 'post',
    url: 'https://reqres.in/api/register',
    data: {
      email,
      password,
    },
    validateStatus: status => status >= 200 && status <= 400,
  });
  return { response: response.data, status: response.status };
};

const userService = {
  userAuth,
  userRegister,
};

export default userService;
