import axios from 'axios';

const getCharacters = async (currentPage, filterName) => {
  const response = await axios({
    method: 'get',
    url: 'https://rickandmortyapi.com/api/character/',
    params: {
      page: currentPage,
      name: filterName,
    },
    validateStatus: status => status >= 200 && status <= 404,
  });
  return { response: response.data, status: response.status };
};

const getSingleCharacter = async id => {
  const response = await axios({
    method: 'get',
    url: `https://rickandmortyapi.com/api/character/${id}`,
  });
  return { response: response.data };
};

const getLocations = async (currentPage, filterName) => {
  const response = await axios({
    method: 'get',
    url: 'https://rickandmortyapi.com/api/location/',
    params: {
      page: currentPage,
      name: filterName,
    },
    validateStatus: status => status >= 200 && status <= 404,
  });
  return { response: response.data, status: response.status };
};

const getSingleLocation = async id => {
  const response = await axios({
    method: 'get',
    url: `https://rickandmortyapi.com/api/location/${id}`,
  });
  return { response: response.data };
};

const getEpisodes = async episodes => {
  const response = await axios({
    method: 'get',
    url: `https://rickandmortyapi.com/api/episode/${episodes}`,
  });
  return { response: response.data };
};

const getSingleEpisode = async id => {
  const response = await axios({
    method: 'get',
    url: `https://rickandmortyapi.com/api/episode/${id}`,
  });
  return { response: response.data };
};

const rickAndMortyService = {
  getCharacters,
  getSingleCharacter,
  getLocations,
  getSingleLocation,
  getEpisodes,
  getSingleEpisode,
};

export default rickAndMortyService;
