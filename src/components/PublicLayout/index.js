import React from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col } from 'react-bootstrap';
import './PublicLayout.scss';

const PublicLayout = ({ children }) => {
  return (
    <Container fluid className="public-container">
      <Row className="min-vh-100">
        <Col
          className="align-self-center public-center-container p-5"
          align="center"
          xl={{ span: 4, offset: 4 }}
          lg={{ span: 6, offset: 3 }}
          md={{ span: 6, offset: 3 }}
          sm={{ span: 10, offset: 1 }}
          xs={{ span: 10, offset: 1 }}
        >
          {children}
        </Col>
      </Row>
    </Container>
  );
};

PublicLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default PublicLayout;
