import React from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col } from 'react-bootstrap';
import './PrivateLayout.scss';
import Menu from '../Menu';

const PrivateLayout = ({ children }) => {
  return (
    <Container fluid className="private-container">
      <Row>
        <Menu />
      </Row>
      <Row>
        <Col span={12}>{children}</Col>
      </Row>
    </Container>
  );
};

PrivateLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default PrivateLayout;
