import React from 'react';
import { Button, Form } from 'react-bootstrap';
import { Formik } from 'formik';
import PropTypes from 'prop-types';
import schema from '../../utils/validationSchema';

const EmailPasswordForm = ({ onSubmit, isLoading, buttonName }) => {
  return (
    <div className="w-75">
      <Formik
        validationSchema={schema}
        onSubmit={values => onSubmit(values)}
        initialValues={{
          email: '',
          password: '',
        }}
      >
        {({ handleSubmit, handleChange, handleBlur, values, touched, errors }) => (
          <Form noValidate onSubmit={handleSubmit}>
            <Form.Group style={{ minHeight: '60px' }} className="mb-1">
              <Form.Control
                type="text"
                name="email"
                placeholder="Email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
                isInvalid={!!errors.email && !!touched.email}
              />
              <Form.Control.Feedback className="text-left ml-1 mt-0" type="invalid">
                {errors.email}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group style={{ minHeight: '60px' }}>
              <Form.Control
                type="password"
                name="password"
                placeholder="Password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
                isInvalid={!!errors.password && !!touched.password}
              />
              <Form.Control.Feedback className="text-left ml-1 mt-0" type="invalid">
                {errors.password}
              </Form.Control.Feedback>
            </Form.Group>
            <Button disabled={isLoading} variant="main" type="submit" className="w-100">
              {isLoading ? 'Loading…' : buttonName}
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

EmailPasswordForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  buttonName: PropTypes.string,
};

EmailPasswordForm.defaultProps = {
  buttonName: 'Log in',
};

export default EmailPasswordForm;
