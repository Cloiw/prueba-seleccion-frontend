import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Navbar, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import * as userActions from '../../store/slices/userSlice';
import './Menu.scss';

const Menu = ({ logout }) => {
  return (
    <Navbar variant="main" className="w-100">
      <Link className="nav-brand" to="/home">
        <h2>Rick and Morty</h2>
      </Link>
      <Navbar.Collapse className="justify-content-end">
        <Navbar.Text>
          <Button onClick={logout} variant="blue">
            Logout
          </Button>
        </Navbar.Text>
      </Navbar.Collapse>
    </Navbar>
  );
};

const mapDispatchToProps = dispatch => ({
  logout: () => {
    dispatch(userActions.logoutUser());
  },
});

Menu.propTypes = {
  logout: PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(Menu);
