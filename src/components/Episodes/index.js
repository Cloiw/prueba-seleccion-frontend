import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import * as episodesActions from '../../store/slices/episodesSlice';
import EpisodesTable from '../EpisodesTable';
import SearchRow from '../SearchRow';

const Episodes = ({
  getFirstSeasonEpisodes,
  episodesFirstSeason,
  episodesFirstSeasonLoading,
  getSecondSeasonEpisodes,
  episodesSecondSeason,
  episodesSecondSeasonLoading,
  getThirdSeasonEpisodes,
  episodesThirdSeason,
  episodesThirdSeasonLoading,
  setSearchValue,
  searchValue,
  nonFilteredFirstSeason,
  nonFilteredSecondSeason,
  nonFilteredThirdSeason,
}) => {
  const [isDataFilled, setDataFilled] = useState(false);

  const handleRequests = () => {
    getFirstSeasonEpisodes();
    getSecondSeasonEpisodes();
    getThirdSeasonEpisodes();
  };

  useEffect(handleRequests, [
    getFirstSeasonEpisodes,
    getSecondSeasonEpisodes,
    getThirdSeasonEpisodes,
  ]);

  const handleSearchValue = () => {
    const listsAreNotEmpty =
      !!nonFilteredFirstSeason.length &&
      !!nonFilteredSecondSeason.length &&
      !!nonFilteredThirdSeason.length;
    if (listsAreNotEmpty && !isDataFilled) {
      setDataFilled(true);
      setSearchValue(searchValue);
    }
  };

  useEffect(handleSearchValue, [
    nonFilteredFirstSeason,
    nonFilteredSecondSeason,
    nonFilteredThirdSeason,
  ]);

  const handleChange = e => {
    setSearchValue(e.target.value.toLowerCase());
  };

  return (
    <>
      <SearchRow handleChange={handleChange} searchValue={searchValue} />
      <Row noGutters className="justify-content-center my-4">
        <Col sm={12} md={6} lg={4}>
          <EpisodesTable
            title="First season"
            data={episodesFirstSeason}
            isLoading={episodesFirstSeasonLoading}
          />
        </Col>
        <Col sm={12} md={6} lg={4}>
          <EpisodesTable
            title="Second season"
            data={episodesSecondSeason}
            isLoading={episodesSecondSeasonLoading}
          />
        </Col>
        <Col sm={12} md={6} lg={4}>
          <EpisodesTable
            title="Third season"
            data={episodesThirdSeason}
            isLoading={episodesThirdSeasonLoading}
          />
        </Col>
      </Row>
    </>
  );
};

const mapStateToProps = state => ({
  episodesFirstSeason: state.episodes.filteredFirstSeason,
  episodesFirstSeasonLoading: state.episodes.episodesFirstSeasonLoading,
  episodesSecondSeason: state.episodes.filteredSecondSeason,
  episodesSecondSeasonLoading: state.episodes.episodesSecondSeasonLoading,
  episodesThirdSeason: state.episodes.filteredThirdSeason,
  episodesThirdSeasonLoading: state.episodes.episodesThirdSeasonLoading,
  searchValue: state.episodes.searchValue,
  nonFilteredFirstSeason: state.episodes.episodesFirstSeasonList,
  nonFilteredSecondSeason: state.episodes.episodesSecondSeasonList,
  nonFilteredThirdSeason: state.episodes.episodesThirdSeasonList,
});

const mapDispatchToProps = dispatch => ({
  getFirstSeasonEpisodes: () => {
    dispatch(episodesActions.getFirstSeasonEpisodes());
  },
  getSecondSeasonEpisodes: () => {
    dispatch(episodesActions.getSecondSeasonEpisodes());
  },
  getThirdSeasonEpisodes: () => {
    dispatch(episodesActions.getThirdSeasonEpisodes());
  },
  setSearchValue: value => {
    dispatch(episodesActions.setSearchValue(value));
  },
});

Episodes.propTypes = {
  getFirstSeasonEpisodes: PropTypes.func.isRequired,
  getSecondSeasonEpisodes: PropTypes.func.isRequired,
  getThirdSeasonEpisodes: PropTypes.func.isRequired,

  episodesFirstSeason: PropTypes.arrayOf(PropTypes.any).isRequired,
  episodesFirstSeasonLoading: PropTypes.bool.isRequired,
  episodesSecondSeason: PropTypes.arrayOf(PropTypes.any).isRequired,
  episodesSecondSeasonLoading: PropTypes.bool.isRequired,
  episodesThirdSeason: PropTypes.arrayOf(PropTypes.any).isRequired,
  episodesThirdSeasonLoading: PropTypes.bool.isRequired,

  searchValue: PropTypes.string.isRequired,
  setSearchValue: PropTypes.func.isRequired,

  nonFilteredFirstSeason: PropTypes.arrayOf(PropTypes.any).isRequired,
  nonFilteredSecondSeason: PropTypes.arrayOf(PropTypes.any).isRequired,
  nonFilteredThirdSeason: PropTypes.arrayOf(PropTypes.any).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Episodes);
