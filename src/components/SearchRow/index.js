import React from 'react';
import PropTypes from 'prop-types';
import { InputGroup, FormControl, Row, Col } from 'react-bootstrap';
import { GoSearch } from 'react-icons/go';

const SearchRow = ({ handleChange, searchValue }) => (
  <Row noGutters className="my-4 d-flex justify-content-end">
    <Col md={6} xs={12}>
      <InputGroup className="search-input" tabIndex="-1">
        <FormControl
          value={searchValue}
          onChange={handleChange}
          className="border-0"
          placeholder="Search by name"
        />
        <InputGroup.Append className="align-items-center p-2">
          <GoSearch size={20} color="gray" />
        </InputGroup.Append>
      </InputGroup>
    </Col>
  </Row>
);

SearchRow.propTypes = {
  handleChange: PropTypes.func.isRequired,
  searchValue: PropTypes.string.isRequired,
};

export default SearchRow;
