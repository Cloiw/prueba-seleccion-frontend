import React from 'react';
import { useHistory } from 'react-router-dom';
import { Row, Button } from 'react-bootstrap';
import { FaArrowLeft } from 'react-icons/fa';

const BackButton = () => {
  const history = useHistory();
  return (
    <Row className="px-5 py-3">
      <Button
        variant="blue"
        className="d-flex align-items-center"
        onClick={() => history.push('/home')}
      >
        <FaArrowLeft size={20} color="09d3b1" className="mr-2" />
        Back
      </Button>
    </Row>
  );
};

export default BackButton;
