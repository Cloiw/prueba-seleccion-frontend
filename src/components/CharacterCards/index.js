import React from 'react';
import PropTypes from 'prop-types';
import { Card, Button, CardGroup } from 'react-bootstrap';
import './CharacterCards.scss';
import { useHistory } from 'react-router-dom';

const CharacterCards = ({ characters }) => {
  const history = useHistory();

  return (
    <CardGroup className="justify-content-center d-flex flex-wrap">
      {characters.map(character => (
        <Card key={character.id} className="m-1 character-card">
          <Card.Img variant="top" src={character.image} />
          <Card.ImgOverlay className="p-0">
            <div style={{ height: '70%' }} className="p-0 d-flex">
              <span className="align-self-end pl-1 character-card-name">
                {character.name}
              </span>
            </div>
          </Card.ImgOverlay>
          <Card.Footer className="p-1 d-flex justify-content-center">
            <Button
              onClick={() => history.push(`/detail_character/${character.id}`)}
              variant="detail"
            >
              Details
            </Button>
          </Card.Footer>
        </Card>
      ))}
    </CardGroup>
  );
};

CharacterCards.propTypes = {
  characters: PropTypes.arrayOf(PropTypes.any).isRequired,
};

export default CharacterCards;
