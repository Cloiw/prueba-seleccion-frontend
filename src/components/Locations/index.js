import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Table, Row, Col, Spinner, Button } from 'react-bootstrap';
import Pagination from 'react-js-pagination';
import useDebounce from '../../hooks/useDebounce';
import * as locationsActions from '../../store/slices/locationsSlice';
import SearchRow from '../SearchRow';

const Locations = ({
  getLocations,
  setCurrentPage,
  currentPage,
  setSearchValue,
  searchValue,
  locations,
  totalLocations,
  loading,
  error,
}) => {
  const history = useHistory();
  const debouncedSearchValue = useDebounce(searchValue, 300);

  const handlePageChange = newCurrentPage => {
    if (newCurrentPage !== currentPage) {
      setCurrentPage(newCurrentPage);
    }
  };

  const handleChange = e => {
    setSearchValue(e.target.value);
    if (currentPage !== 1) {
      setCurrentPage(1);
    }
  };

  useEffect(() => {
    if (debouncedSearchValue === searchValue) {
      getLocations(currentPage, searchValue);
    }
  }, [getLocations, currentPage, searchValue, debouncedSearchValue]);

  return (
    <>
      <SearchRow handleChange={handleChange} searchValue={searchValue} />
      {error.type ? (
        <span className="text-blue">{error.type}</span>
      ) : (
        <>
          <Row noGutters className="justify-content-center">
            {loading ? (
              <Spinner animation="grow" variant="light" />
            ) : (
              <Table hover size="sm" className="main-table">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th> </th>
                  </tr>
                </thead>
                <tbody>
                  {locations.map(location => (
                    <tr key={location.id}>
                      <td>{location.name}</td>
                      <td align="center">
                        <Button
                          onClick={() => history.push(`/detail_location/${location.id}`)}
                          variant="detail"
                        >
                          Details
                        </Button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            )}
          </Row>
          {!loading && (
            <Row>
              <Col className="d-flex justify-content-end pt-2">
                <Pagination
                  itemClass="page-item"
                  linkClass="page-link"
                  activeLinkClass="active"
                  hideNavigation
                  pageRangeDisplayed={4}
                  activePage={currentPage}
                  itemsCountPerPage={20}
                  totalItemsCount={totalLocations}
                  onChange={handlePageChange}
                />
              </Col>
            </Row>
          )}
        </>
      )}
    </>
  );
};

const mapStateToProps = state => ({
  locations: state.locations.locationsList,
  loading: state.locations.locationsLoading,
  totalLocations: state.locations.totalDocs,
  currentPage: state.locations.currentPage,
  searchValue: state.locations.searchValue,
  error: state.locations.locationsError,
});

const mapDispatchToProps = dispatch => ({
  getLocations: (currentPage, filterName) => {
    dispatch(locationsActions.getLocations(currentPage, filterName));
  },
  setCurrentPage: page => {
    dispatch(locationsActions.setCurrentPage(page));
  },
  setSearchValue: value => {
    dispatch(locationsActions.setSearchValue(value));
  },
});

Locations.propTypes = {
  locations: PropTypes.arrayOf(PropTypes.any).isRequired,
  loading: PropTypes.bool.isRequired,
  totalLocations: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  searchValue: PropTypes.string.isRequired,
  error: PropTypes.oneOfType([PropTypes.object]).isRequired,
  getLocations: PropTypes.func.isRequired,
  setCurrentPage: PropTypes.func.isRequired,
  setSearchValue: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Locations);
