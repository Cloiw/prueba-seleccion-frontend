import React from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Table, Row, Spinner, Button } from 'react-bootstrap';

const EpisodesTable = ({ title, data, isLoading }) => {
  const history = useHistory();
  return (
    <Row noGutters className="justify-content-center p-2">
      {isLoading ? (
        <Spinner animation="grow" variant="light" />
      ) : (
        <>
          <h4 className="text-blue">{title}</h4>
          <Table hover size="sm" className="main-table">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th> </th>
              </tr>
            </thead>
            <tbody>
              {data.length ? (
                data.map(episode => (
                  <tr key={episode.id}>
                    <td>{episode.episode.slice(-2)}</td>
                    <td>{episode.name}</td>
                    <td align="center">
                      <Button
                        onClick={() => history.push(`/detail_episode/${episode.id}`)}
                        variant="detail"
                      >
                        Details
                      </Button>
                    </td>
                  </tr>
                ))
              ) : (
                <tr align="middle">
                  <td>There is nothing here</td>
                </tr>
              )}
            </tbody>
          </Table>
        </>
      )}
    </Row>
  );
};

EpisodesTable.propTypes = {
  title: PropTypes.string,
  data: PropTypes.arrayOf(PropTypes.any).isRequired,
  isLoading: PropTypes.bool,
};

EpisodesTable.defaultProps = {
  title: 'Table',
  isLoading: false,
};

export default EpisodesTable;
