import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col, Spinner } from 'react-bootstrap';
import Pagination from 'react-js-pagination';
import useDebounce from '../../hooks/useDebounce';
import CharacterCards from '../CharacterCards';
import * as charactersActions from '../../store/slices/charactersSlice';
import SearchRow from '../SearchRow';

const Characters = ({
  getCharacters,
  setCurrentPage,
  currentPage,
  setSearchValue,
  searchValue,
  characters,
  totalCharacters,
  loading,
  error,
}) => {
  const debouncedSearchValue = useDebounce(searchValue, 300);

  const handlePageChange = newCurrentPage => {
    if (newCurrentPage !== currentPage) {
      setCurrentPage(newCurrentPage);
    }
  };

  const handleChange = e => {
    setSearchValue(e.target.value);
    if (currentPage !== 1) {
      setCurrentPage(1);
    }
  };

  useEffect(() => {
    if (debouncedSearchValue === searchValue) {
      getCharacters(currentPage, searchValue);
    }
  }, [getCharacters, currentPage, searchValue, debouncedSearchValue]);

  return (
    <>
      <SearchRow handleChange={handleChange} searchValue={searchValue} />
      {error.type ? (
        <span className="text-blue">{error.type}</span>
      ) : (
        <>
          <Row className="justify-content-center">
            {loading ? (
              <Spinner animation="grow" variant="light" />
            ) : (
              <CharacterCards characters={characters} />
            )}
          </Row>
          {!loading && (
            <Row>
              <Col className="d-flex justify-content-end pt-2">
                <Pagination
                  itemClass="page-item"
                  linkClass="page-link"
                  activeLinkClass="active"
                  hideNavigation
                  pageRangeDisplayed={4}
                  activePage={currentPage}
                  itemsCountPerPage={20}
                  totalItemsCount={totalCharacters}
                  onChange={handlePageChange}
                />
              </Col>
            </Row>
          )}
        </>
      )}
    </>
  );
};

const mapStateToProps = state => ({
  characters: state.characters.charactersList,
  loading: state.characters.charactersLoading,
  totalCharacters: state.characters.totalDocs,
  currentPage: state.characters.currentPage,
  searchValue: state.characters.searchValue,
  error: state.characters.charactersError,
});

const mapDispatchToProps = dispatch => ({
  getCharacters: (currentPage, filterName) => {
    dispatch(charactersActions.getCharacters(currentPage, filterName));
  },
  setCurrentPage: page => {
    dispatch(charactersActions.setCurrentPage(page));
  },
  setSearchValue: value => {
    dispatch(charactersActions.setSearchValue(value));
  },
});

Characters.propTypes = {
  characters: PropTypes.arrayOf(PropTypes.any).isRequired,
  getCharacters: PropTypes.func.isRequired,
  setCurrentPage: PropTypes.func.isRequired,
  setSearchValue: PropTypes.func.isRequired,
  currentPage: PropTypes.number.isRequired,
  searchValue: PropTypes.string.isRequired,
  totalCharacters: PropTypes.number.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.oneOfType([PropTypes.object]).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Characters);
