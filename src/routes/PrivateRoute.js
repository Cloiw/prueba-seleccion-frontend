import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import PrivateLayout from '../components/PrivateLayout';

const PrivateRoute = ({ exact, path, component: Component, isUserAuthenticated }) => {
  if (!isUserAuthenticated) return <Redirect exact to="/login" />;
  return (
    <Route
      exact={exact}
      path={path}
      render={() => (
        <PrivateLayout>
          <Component />
        </PrivateLayout>
      )}
    />
  );
};

const mapStateToProps = state => ({
  isUserAuthenticated: state.user.isUserAuthenticated,
});

PrivateRoute.propTypes = {
  exact: PropTypes.bool,
  path: PropTypes.string.isRequired,
  component: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
  isUserAuthenticated: PropTypes.bool,
};

PrivateRoute.defaultProps = {
  exact: false,
  isUserAuthenticated: false,
};

export default connect(mapStateToProps)(PrivateRoute);
