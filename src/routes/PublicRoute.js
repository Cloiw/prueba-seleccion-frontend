import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import PublicLayout from '../components/PublicLayout';

const PublicRoute = ({ exact, path, component: Component, isUserAuthenticated }) => {
  if (isUserAuthenticated) return <Redirect exact to="/home" />;
  return (
    <Route
      exact={exact}
      path={path}
      render={() => (
        <PublicLayout>
          <Component />
        </PublicLayout>
      )}
    />
  );
};

const mapStateToProps = state => ({
  isUserAuthenticated: state.user.isUserAuthenticated,
});

PublicRoute.propTypes = {
  exact: PropTypes.bool,
  path: PropTypes.string.isRequired,
  component: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
  isUserAuthenticated: PropTypes.bool,
};

PublicRoute.defaultProps = {
  exact: false,
  isUserAuthenticated: false,
};

export default connect(mapStateToProps)(PublicRoute);
