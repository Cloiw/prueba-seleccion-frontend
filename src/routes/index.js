import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import Login from '../views/Login';
import Register from '../views/Register';
import Home from '../views/Home';
import DetailCharacter from '../views/DetailCharacter';
import DetailLocation from '../views/DetailLocation';
import DetailEpisode from '../views/DetailEpisode';
import PublicRoute from './PublicRoute';
import PrivateRoute from './PrivateRoute';

const Routes = () => {
  return (
    <Switch>
      <PublicRoute exact path="/login" component={Login} />
      <PublicRoute exact path="/register" component={Register} />
      <PrivateRoute exact path="/home" component={Home} />
      <PrivateRoute exact path="/detail_character/:id" component={DetailCharacter} />
      <PrivateRoute exact path="/detail_location/:id" component={DetailLocation} />
      <PrivateRoute exact path="/detail_episode/:id" component={DetailEpisode} />
      <Redirect to="/login" />
    </Switch>
  );
};

export default Routes;
